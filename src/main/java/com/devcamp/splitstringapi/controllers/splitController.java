package com.devcamp.splitstringapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class splitController {
    @GetMapping("/split")
    public ArrayList<String> arrayList(@RequestParam(required = true, name = "string") String requestString) {
        String[] arrayList = requestString.split(" ");
        ArrayList<String> resultArray = new ArrayList<>();
        for (String s : arrayList) {
            resultArray.add(s);
        }
        return resultArray;
    }

}
